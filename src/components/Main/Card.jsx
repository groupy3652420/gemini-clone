/* eslint-disable react/prop-types */
import { useContext } from "react";
import { Context } from "../../context/Context";

const Card = (props) => {
  const { loadPrompt } = useContext(Context);
  const { cardPrompt, icon } = props;
  return (
    <div onClick={() => loadPrompt(cardPrompt)} className="card">
      <p>{cardPrompt}</p>
      <img src={icon} alt="" />
    </div>
  );
};

export default Card;
